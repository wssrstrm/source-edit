var dom      = require("ace/lib/dom")
var commands = require("ace/commands/default_commands").commands

/**
 * Define Ace Editor Defaults
 */
commands.push({
    name: "Toggle Fullscreen",
    bindKey: "ESC",
    exec: function(editor) {
        var jeditor = $(editor.container);
        var id      = jeditor.attr('id');

        if (jeditor.hasClass('fullscreen')) {
            console.log(jeditor.attr('data-container-id'));
            jeditor.appendTo('#' + jeditor.attr('data-container-id')).css({
                width: '100%',
                height: $(jeditor).parent().css('height'),
                zIndex: 'auto',
                top: 'auto',
                left: 'auto',
                position: 'relative'
            });
        } else {
            jeditor.appendTo('body').css({
                top: 0,
                left: 0,
                height: '100%',
                zIndex: 50,
                position: 'fixed'
            });
        }

        jeditor.toggleClass('fullscreen');

        editor.resize();
        editor.focus();
    }
});

/**
 * Init Source Edit field type
 * 
 * @param  string  id         
 * @param  string  theme      
 * @param  boolean spaces     
 * @param  int     indentUnit 
 */
function sourceEdit(id, theme, spaces, indentUnit) {
    // Initialize editor
    var se_editor = ace.edit(id);
    se_editor.setTheme("ace/theme/" + theme);
    se_editor.getSession().setMode("ace/mode/html");
    se_editor.setShowPrintMargin(false);

    // Set tab mode and size
    if (spaces) {
        se_editor.getSession().setUseSoftTabs(true);
        se_editor.getSession().setTabSize(indentUnit);
    }

    // Tell editor to update textarea on change
    se_editor.getSession().on('change', function(){
        document.getElementById(id + '-input').value = se_editor.getSession().getValue();
    });
}
