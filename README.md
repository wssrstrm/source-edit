# Source Edit Field Type for Craft CMS

Source Edit integrates the ACE code editor into a field type.  This will allow you to set up fields where you can
directly edit HTML.  This plugin was primarily designed as a means to manging content on my own site (I wanted somewhere
to edit code, but didn't want to use a WYSIWYG editor).

## Options

The available options for this plugin are:

* Tabs as spaces
* Indent Unit (number of spaces to replace a tab)
* Height of editor window
* Editor theme

## How to Install

1. Place the sourceedit folder in your craft/plugins/ folder.
2. Within your craft admin panel, go to Settings > Plugins.
3. Enable the Source Edit plugin.
3. Create a field and select "Source Edit" as the field type. You can set all plugin options on a per-field basis.